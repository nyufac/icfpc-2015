{-# LANGUAGE OverloadedStrings #-}
module Data.Game where

-- Как много все таки в хаскеле приходится скрывать.
import Data.List hiding (union)
import Data.Maybe
import Data.Set (Set, fromList, member, union, toList, isSubsetOf)

import Prelude 						hiding (readFile, putStrLn)
import Control.Applicative
import Control.Monad

import Data.ByteString.Lazy.Char8 	hiding (readFile)
import Data.ByteString.Lazy 		hiding (putStrLn, unpack)
import Data.Aeson

import Debug.Trace (traceShow)

data Game = Game {
	  id 			:: Int
	, units 		:: [Unit]
	, width 		:: Int
	, height 		:: Int
	, filled 		:: [Cell]
	, sourceLength 	:: Int
	, sourceSeeds   :: [Int]
} deriving Show

data Unit = Unit {
	 pivot 			:: Cell
	, members 		:: Set Cell
} deriving (Show, Eq, Ord)

data Cell = Cell {
	  x				:: Int
	, y 			:: Int
} deriving (Show, Eq, Ord)

data Solution = Solution {
	  problemId 	:: Int
	, seed 			:: Int
	, tag			:: String
	, solution 		:: String
} deriving Show

data GameField = GameField {
		  fieldWidth 	:: Int
		, fieldHeight 	:: Int
		, filledCells  	:: Set Cell
	} deriving Show

mkField w h filled = GameField w h (fromList filled)

validCell (GameField w h _) (Cell x y) =  x < w && y < h && x >= 0 && y >= 0 

validUnit field@(GameField w h filled) cell@(Unit p members) = -- traceShow (cell, field) $
	(Prelude.all (\c -> (validCell field c) && (not $ member c filled)) (toList members))

data Trace = Trace {
		 prevs     :: Set Unit
		, unit     :: Unit
	 	, commands :: [Command]
	 	, tseed    :: Int
	} deriving Show

nullTrace u s = Trace (fromList [u]) u [] s


data GameState = GameState {
		  sfield 	:: GameField
		, sunit 	:: Unit
	} deriving Show

instance ToJSON GameField where
	toJSON (GameField w h filled) = object [
			  "height" .= h
			, "width"  .= w
			, "filled" .= toList filled
		] 

instance ToJSON GameState where
	toJSON (GameState f u) = object [
			  "field" .= f
			, "unit"  .= u
		]

instance ToJSON Cell where
	toJSON (Cell x y) = object [
			  "x" .= x
			, "y" .= y
		]

instance ToJSON Unit where
	toJSON (Unit pivot members) = object [
			  "pivot"   .= pivot
			, "members" .= members
		]

instance ToJSON Solution where
	toJSON (Solution id seed tag solution) = object [
			  "problemId" .= id
			, "seed" 	  .= seed
			, "tag" 	  .= tag
			, "solution"  .= solution
		]

instance FromJSON Game where 
	parseJSON (Object v) = Game <$>
						   v .: "id" 			<*>
						   v .: "units" 		<*>
						   v .: "width" 		<*>
						   v .: "height" 		<*>
						   v .: "filled" 		<*>
						   v .: "sourceLength" 	<*>
						   v .: "sourceSeeds" 	
	parseJSON _ 		= mzero

instance FromJSON Unit where 
	parseJSON (Object v) = Unit <$>
						   v .: "pivot"			<*>
						   v .: "members" 		
	parseJSON _ 		= mzero

instance FromJSON Cell where 
	parseJSON (Object v) = Cell <$>
						   v .: "x"				<*>
						   v .: "y" 		
	parseJSON _ 		= mzero

readGameInput :: FilePath -> IO (Maybe Game)
readGameInput path = readFile path >>= return . decode

printGameOutput :: [Solution] -> IO ()
printGameOutput sol = putStrLn (encode sol) 

showGameState :: GameState -> String
showGameState = unpack . encode 

showSolution :: Solution -> String
showSolution = unpack . encode 

data Command = MoveW | MoveE | MoveSW | MoveSE | RotCW | RotCCW
	deriving Eq

antiCmd MoveW  = MoveE
antiCmd MoveE  = MoveW
antiCmd MoveSW = MoveSE
antiCmd MoveSE = MoveSW
antiCmd RotCW  = RotCCW
antiCmd RotCCW = RotCW

{- {p, ', !, ., 0, 3}	move W
{b, c, e, f, y, 2}	move E
{a, g, h, i, j, 4}	move SW
{l, m, n, o, space, 5}    	move SE
{d, q, r, v, z, 1}	rotate clockwise
{k, s, t, u, w, x}	rotate counter-clockwise
\t, \n, \r	(ignored)-}

-- TODO
instance Show Command where
	show MoveW 		= "p"
	show MoveE  	= "b"
	show MoveSW 	= "a"
	show MoveSE 	= "l"
	show RotCW 		= "d"
	show RotCCW 	= "k"