
all: play_icfp2015

builddep:
	cabal install aeson missingH

play_icfp2015: Programa.hs Data/Game.hs System/Argv.hs Game/Exe.hs Game/Geom.hs  Game/HexRot.hs Game/Rng.hs
	ghc -O2 Programa.hs -o $@

clean:
	rm -fr *.o *.hi Data/*.hi Data/*.o System/*.hi System/*.o Game/*.hi Game/*.o play_icfp2015  Stat

qual: play_icfp2015
	./play_icfp2015  -f qdata/problem_0.json -f qdata/problem_1.json -f qdata/problem_2.json -f qdata/problem_3.json -f qdata/problem_4.json -f qdata/problem_5.json -f qdata/problem_6.json -f qdata/problem_7.json -f qdata/problem_8.json -f qdata/problem_9.json -f qdata/problem_10.json -f qdata/problem_11.json -f qdata/problem_12.json -f qdata/problem_13.json -f qdata/problem_14.json -f qdata/problem_15.json -f qdata/problem_16.json -f qdata/problem_17.json -f qdata/problem_18.json -f qdata/problem_19.json -f qdata/problem_20.json -f qdata/problem_21.json -f qdata/problem_22.json -f qdata/problem_23.json > qualification.json 2> /dev/null

multik: play_icfp2015
	./play_icfp2015 -f qdata/$(M) 2> m
	mv m player/
	cd player && node comp.js && mv mc.json m_$(M)
	rm player/m

multik_hanged: 
	mv m player/
	cd player && node comp.js && mv mc.json m_$(M)
	rm player/m

all_multiks:
	make multik M=problem_0.json
	make multik M=problem_1.json
	make multik M=problem_2.json
	make multik M=problem_3.json
	make multik M=problem_4.json
	make multik M=problem_5.json
	make multik M=problem_6.json
	make multik M=problem_7.json
	make multik M=problem_8.json
	make multik M=problem_9.json
	make multik M=problem_10.json
	make multik M=problem_11.json
	make multik M=problem_12.json
	make multik M=problem_13.json
	make multik M=problem_14.json
	make multik M=problem_15.json
	make multik M=problem_16.json
	make multik M=problem_17.json
	make multik M=problem_18.json
	make multik M=problem_19.json
	make multik M=problem_20.json
	make multik M=problem_21.json
	make multik M=problem_22.json
	make multik M=problem_23.json
	make multik M=problem_24.json

qstat:
	./Stat  -f qdata/problem_0.json -f qdata/problem_1.json -f qdata/problem_2.json -f qdata/problem_3.json -f qdata/problem_4.json -f qdata/problem_5.json -f qdata/problem_6.json -f qdata/problem_7.json -f qdata/problem_8.json -f qdata/problem_9.json -f qdata/problem_10.json -f qdata/problem_11.json -f qdata/problem_12.json -f qdata/problem_13.json -f qdata/problem_14.json -f qdata/problem_15.json -f qdata/problem_16.json -f qdata/problem_17.json -f qdata/problem_18.json -f qdata/problem_19.json -f qdata/problem_20.json -f qdata/problem_21.json -f qdata/problem_22.json -f qdata/problem_23.json > qstat
