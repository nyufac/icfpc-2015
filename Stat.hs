import System.Argv
import Data.Maybe
import Data.Game
import Game.Exe

maxInt = 2^32
maxFloat = 2^^32

data StatVal = StatVal {
	statMin 			:: Float
	, statMax 			:: Float
	, statSum 			:: Float
	, statNum 			:: Int
	, statAvg 			:: Float
} deriving (Show, Eq, Ord)

nullSv = StatVal maxFloat 0 0 0 0

svPlus (StatVal m mx sm num avg) n =
	StatVal
		(min m  n')
		(max mx n')
		(sm  + n')
		(num + 1)
		((sm + n')/(fromIntegral(num+1)))
	where n' = fromIntegral n


data Stat = Stat {
	  statW				:: StatVal
	, statH 			:: StatVal
	, statSlen			:: StatVal
	, statUnits 		:: StatVal
	, statFilled		:: StatVal
	, statFilledPcent	:: StatVal
	, statSeeds			:: StatVal
} deriving (Show)

nullStat = Stat nullSv nullSv nullSv nullSv nullSv nullSv nullSv

statPlusGame (Game id units w h filled slen seeds) (Stat sw sh ssl su sf sfp ss)  =
	Stat (svPlus sw   w)
		 (svPlus sh   w)
		 (svPlus ssl  slen)
		 (svPlus su   (length units))
		 (svPlus sf   (length filled))
		 (svPlus sfp  (fpercent w h (length filled)))
		 (svPlus ss   (length seeds))
 	where fpercent w h c = if c == 0 then 0 else (w*h) `div` c


calcStat games = foldr statPlusGame nullStat games

runSpec :: TaskSpec -> IO Stat
runSpec (TaskSpec files pops mlim tlim) = mapM readGameInput files >>= return . calcStat . catMaybes

main :: IO ()
main = readArgs >>= runSpec >>= print