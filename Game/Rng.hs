module Game.Rng where

import Data.Bits
import Data.List

rng :: Int -> [Int]
rng seed = map cut16_30 $ iterate (\n -> (n * rngMul + rngAdd) `mod` rngModulo) seed 
	where 
		cut16_30 n 	= (n .&. 0x7FFF0000) `div` 0x10000
		rngMul     	= 1103515245
		rngAdd	  	= 12345
		rngModulo   = 2^32