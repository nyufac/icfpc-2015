module Game.Geom where

import Data.Set
import Data.Game
import Game.HexRot

moveUnit (Unit pivot members) RotCW  = Unit pivot (Data.Set.map (moveCell RotCW pivot) members)
moveUnit (Unit pivot members) RotCCW = Unit pivot (Data.Set.map (moveCell RotCCW pivot) members)
moveUnit (Unit pivot members) cmd = Unit (moveCell cmd pivot pivot) (Data.Set.map (moveCell cmd pivot) members)

moveCell MoveW  _ (Cell x y) = Cell (x-1) y
moveCell MoveE  _ (Cell x y) = Cell (x+1) y

moveCell MoveSW _ (Cell x y) = Cell (x+dx) (y+1)
	where dx = if even y then (-1) else 0

moveCell MoveSE _ (Cell x y) = Cell (x+dx) (y+1)
	where dx = if odd y then 1 else 0

moveCell RotCW 	(Cell px py) (Cell x y) = (Cell x' y')
	where
		(x', y') = rotCW (px, py) (x, y)

moveCell RotCCW (Cell px py) (Cell x y) = (Cell x' y')
	where 
		(x', y') = rotCCW (px, py) (x, y)

centerUnit (GameField w h _) (Unit pivot members) = 
	if yMin /= 0
		then error "Y != 0!"
		else (Unit (adjust pivot) (Data.Set.map adjust members))
	where
		cx (Cell x y) = x
		cy (Cell x y) = y
		adjx dx (Cell x y) = (Cell (x+dx) y)
		xMax   = Data.Set.foldr max 0 (Data.Set.map cx members)
		yMin   = Data.Set.foldr min h (Data.Set.map cy members)
		xMin   = Data.Set.foldr min w (Data.Set.map cx members)
		lc     = truncate $ (fi $ xMax - xMin) / 2
		center = w `div` 2 - (if even w then 1 else 0)
		adjust = adjx (center - xMin - lc)

unitMaxX (Unit _ members) = Data.Set.foldr max 0 (Data.Set.map cx members)
	where
		cx (Cell x y) = x

unitMinX (Unit _ members) = Data.Set.foldr min 100000000 (Data.Set.map cx members)
	where
		cx (Cell x y) = x