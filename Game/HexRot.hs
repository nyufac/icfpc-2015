module Game.HexRot where

import Data.List
import Data.Maybe
import Data.Game


fi    = fromIntegral
fit x = fromIntegral $ truncate x
fir x = fromIntegral $ round x

sign :: Float -> Float
sign n = if even (truncate n) then 0 else 1

rotCW :: (Int, Int) -> (Int, Int) -> (Int, Int)
rotCW (px, py) (x, y) = (truncate x', truncate y')
	where 
		(x', y') 		= xyz2xy (pxx-dzz, pyy-dxx, pzz-dyy)
		(xx, yy, zz) 	= xy2xyz(fi x, fi y)
		(pxx, pyy, pzz) = xy2xyz(fi px, fi py)
		(dxx, dyy, dzz) = (xx-pxx, yy-pyy, zz-pzz)

rotCCW :: (Int, Int) -> (Int, Int) -> (Int, Int)
rotCCW (px, py) (x, y) = (truncate x', truncate y')
	where 
		(x', y') 		= xyz2xy (pxx-dyy, pyy-dzz, pzz-dxx)
		(xx, yy, zz) 	= xy2xyz(fi x, fi y)
		(pxx, pyy, pzz) = xy2xyz(fi px, fi py)
		(dxx, dyy, dzz) = (xx-pxx, yy-pyy, zz-pzz)


xy2xyz :: (Float, Float) -> (Float, Float, Float)
xy2xyz (x, y) = (xx, yy, zz)
	where
		xx = x - (y - (sign y)) / 2
		zz = y
		yy = -xx -zz

xyz2xy :: (Float, Float, Float) -> (Float, Float)
xyz2xy (xx, yy, zz) = (x, y)
	where
		x = xx + (zz - (sign zz)) / 2
		y = zz

xy2mbb :: (Float, Float) -> (Float, Float, Float)
xy2mbb (x, y) = (m, b1, b2)
	where 
		b1 = if y > 0 then y else 0.0
		m  = x - (fit $ b1 / 2) - (fit $ b2 / 2) 
		b2 = if y < 0 then y else 0.0

mbb2xy :: (Float, Float, Float) -> (Float, Float)
mbb2xy (m, b1, b2) = (x3, y3)
	where
		(x1, y1) = (m, 0)
		(x2, y2) = let pizdec = (if b1 < 0 then (-1) else 0) 
			in (x1 + (fit $ b1/2) + pizdec, y1+b1)
		(x3, y3) = let superPizdec = if b2 < 0 && even (truncate y2) && odd (truncate b2) then (-1) else 0 
			in (x2 + (fit $ b2/2) + superPizdec, y2-b2)
		invSign x = if x < 0 then 1 else -1

{- testRot (src, tgt) = if (rotCW src) == tgt then Nothing else Just ("FAIL " ++ show(src, tgt, rotCW src))


test = intercalate "\n" $ catMaybes $ map testRot [
		  ((0,0), (0,0))
		, ((1,0), (0,1))
		, ((2,0), (1,2))
		, ((3,0), (1,3))
		, ((4,0), (2,4))
		, ((5,0), (2,5))
		, ((6,0), (3,6))
		, ((0,1), (-1,1))
		, ((1,1), (0,2))
		, ((2,1), (0,3))
		, ((3,1), (1,4))
		, ((4,1), (1,5))
		, ((5,1), (2,6))
		, ((6,1), (2,7))
		, ((0,2), (-2,1))
		, ((1,2), (-1,2))
		, ((2,2), (-1,3))
		, ((3,2), (0,4))
		, ((4,2), (0,5))
		, ((5,2), (1,6))
		, ((6,2), (1,7))
		, ((0,3), (-2,2))
		, ((1,3), (-2,3))
		, ((2,3), (-1,4))
		, ((3,3), (-1,5))
		, ((4,4), (-1,6))
	]

test2 = intercalate "\n" $ catMaybes $ map testRot [
		  ((1,0), 	(0,1))
		, ((0,1), 	(-1,1))
		, ((-1,1), 	(-1,0))
		, ((-1,0), 	(-1,-1))
		, ((-1,-1), (0,-1))
		, ((0,-1), (1,0))
	] -}