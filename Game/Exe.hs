module Game.Exe where

import Data.Char
import Data.List hiding (union)
import Data.List.Utils
import Data.Maybe
import Data.Set (Set, fromList, toList, member, union, insert, notMember)
import Data.Map.Strict hiding (map, foldr, filter, notMember, union, toList)
import Data.Game
import Game.Geom
import Game.Rng

import Debug.Trace (traceShow)

-- Field Updating

updateField (GameField w h filled) (Unit p members) = GameField w h filled''
	where 
		filled''   	   = Data.Set.fromList $ foldr remove filled' fullLines
		filled'   	   = toList $ union filled members
		fullLines      = filter (full filled') [0..h-1]
		full field r   = (length $ filter (\(Cell x y) -> y == r) field) == w
		remove r field = map (\c@(Cell x y) -> if y < r then moveCell (if odd y then MoveSW else MoveSE) c c else c) 
					   $ filter (\(Cell x y) -> y /= r) field

-- Units Tracing & Stepping

chooseNextStep field prevs unit preferred seed = next
	where
		checkCmd c = let moved = moveUnit unit c in
			validUnit field moved && notMember moved prevs
		next 		= find checkCmd actions
		actions 	= 
			let actionsLR = case seed `mod` 4 of 
				0 -> [MoveSW, RotCW, RotCCW, MoveSE, MoveW, MoveE]
				1 -> [MoveSW, RotCW, RotCCW, MoveSE, MoveE, MoveW]
				2 -> [MoveSE, RotCW, RotCCW, MoveSW, MoveE, MoveW]
				3 -> [MoveSE, RotCW, RotCCW, MoveSW, MoveW, MoveE]
			in (preferred++actionsLR)

chooseEndCommand field unit = fromMaybe (error "Cannot End!") $ find checkCmd actions
	where
		checkCmd c = let moved = moveUnit unit c in
			not $ validUnit field moved
		actions = [MoveSW, RotCW, RotCCW, MoveSE, MoveW, MoveE]

traceUnit pops unit (traces, field, seed, targetX) = 
	if validUnit field centered
		then trace (nullTrace centered seed)
		else (traces, field, seed, targetX)
	where
		centered = centerUnit field unit
		trace tr@(Trace prevs u commands s) =
			let dx 			= targetX - (unitMinX unit) in
			let preferred 	= if dx < 0 
				then [MoveSE,MoveE] 
				else [MoveSW,MoveW] in
			case traceShow (showGameState (GameState field u)) $ (chooseNextStep field prevs u [] s) of 
				Just cmd -> 
					let moved = (moveUnit u cmd)
					in  trace (Trace (Data.Set.insert moved prevs) moved (cmd:commands) (s+1))
				Nothing  -> let endCommand = chooseEndCommand field u 
					in ((concatMap show $ reverse (endCommand:commands)):traces, updateField field u, seed, unitMaxX u)
		

traceGame pops field source seed = reverse traces
	where (traces, rfield, _, _) = foldr (traceUnit pops) ([], field, seed, 0) (reverse source)

-- Playing games

play :: [String] -> Maybe Game -> [Solution]
play pops (Just (Game id units w h filled slen seeds)) = map (playSeed id field units slen pops) seeds
	where 
		field  = mkField w h filled

play pops Nothing     = []

playSeed id field units slen pops seed = traceShow (showSolution solution) $ solution 
	where 
		solution = Solution 
			id 
			seed 
			"PoPs" 
			(injectPops popsN $ intercalate "" (traceGame popsN field source seed))
		source  = take slen $ map (sel units) (rng seed)
		sel l n = l !! (n `mod` (length l))
		popsN   = map (\pop -> (normalize pop, pop)) pops


injectPops popsN str = foldr (\(pn, p) -> replace pn p) str popsN

normalize = map (\c -> abc ! (toLower c))  
	where 
		mkPairs l@(x:xs) = map (\c -> (c, x)) l
		abc = Data.Map.Strict.fromList $
			mkPairs	"p'!.03"
			++ mkPairs	"bcefy2"
			++ mkPairs	"aghij4"
			++ mkPairs	"lmno 5"
			++ mkPairs	"dqrvz1"
			++ mkPairs	"kstuwx"