module System.Argv where

import Control.Applicative
import Control.Monad
import Data.Char
import Data.List
import System.Console.GetOpt
import System.Environment
import System.Exit
import System.IO

data TaskSpec = TaskSpec {
	  files  :: [String]
	, pops   :: [String]
	, mlimit :: Maybe Int
	, tlimit :: Maybe Int
} deriving (Show)

data Flag
    = FlagFile    	String  -- -f
    | FlagTlimit 	String  -- -t
    | FlagMlimit 	String  -- -m
    | FlagPoP 		String  -- -p
    deriving (Eq,Show)

flags = [
		  Option ['f'] ["file-name"]		(ReqArg FlagFile 		"Input Filename")
			"Specifies input file to process"
		, Option ['t'] ["time-limit"] 		(ReqArg FlagTlimit 		"Time Limit")
			"Specifies time limit"	
		, Option ['m'] ["memory-limit"] 	(ReqArg FlagMlimit 		"Memory Limit")
			"Specifies memory limit"
		, Option ['p'] ["phrase-of-power"] 	(ReqArg FlagPoP 		"Phrase of Power")
			"Specifies phrase of power"
	]

selList flag = \x l ->  if x == flag (arg x) then (arg x):l else l
	where arg s = case s of 
		FlagFile 	x -> x 
		FlagTlimit 	x -> x 
		FlagMlimit 	x -> x 
		FlagPoP 	x -> x 

selInt flag = \x l ->  if x == flag (arg x) then Just (read (arg x) :: Int) else l
	where arg s = case s of 
		FlagFile 	x -> x 
		FlagTlimit 	x -> x 
		FlagMlimit 	x -> x 
		FlagPoP 	x -> x 


makeSpec :: [Flag] -> TaskSpec
makeSpec flags = TaskSpec
				   (foldr (selList FlagFile) 	[] 		flags)
				   (foldr (selList FlagPoP) 	[] 		flags)
				   (foldr (selInt  FlagMlimit) 	Nothing flags)
				   (foldr (selInt  FlagTlimit) 	Nothing flags)

parseArgv :: [String] -> IO TaskSpec
parseArgv argv = do
	popsRaw <- readFile "pops.txt"
	let pops = concatMap (\pop -> ["-p", pop]) (lines popsRaw)
	case getOpt Permute flags (argv ++ pops) of
	 
	    (args,fs,[]) -> do
	        let files = if null fs then ["-"] else fs
	        return $ makeSpec args
	        
	    (_,_,errs)      -> do
	        hPutStrLn stderr (concat errs)
	        exitWith (ExitFailure 1)
	

readArgs :: IO TaskSpec
readArgs = getArgs >>= parseArgv