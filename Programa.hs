import System.Argv
import Data.Game
import Game.Exe

runSpec :: TaskSpec -> IO [Solution]
runSpec (TaskSpec files pops mlim tlim) = mapM readGameInput files >>= return . (concatMap (play pops))

main :: IO ()
main = readArgs >>= runSpec >>= printGameOutput