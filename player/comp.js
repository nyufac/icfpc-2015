var solutions 	= {}
  , video 		= []
  , lastfilled  = null

function diffFrame(pf, frame) {
    if (!pf)
        return frame;
    var diff = [[],[]], pfxy = {}, fxy = {}, i;
    pf.forEach(function (xy) { pfxy[xy.x+':'+xy.y] = xy; })
    frame.field.filled.forEach(function (xy) { fxy[xy.x+':'+xy.y] = xy; })
    for (i in pfxy) 
        if (!fxy[i])
            diff[1].push(pfxy[i])
    for (i in fxy) 
        if (!pfxy[i])
            diff[0].push(fxy[i])
    return {
        unit:       frame.unit
        , filled:   diff[0]
        , unfilled: diff[1]
    }
}

var fs = require('fs'),
    readline = require('readline'),
    read_i = 0;

var rd = readline.createInterface({
    input: fs.createReadStream('m'),
    output: process.stdout,
    terminal: false
});

rd.on('line', function(line) {
	read_i++;
	if (read_i % 100 == 1) {
		process.stdout.write('.');
		//process.stdout.flush();
	}
    var obj=JSON.parse(eval(line));
	if (obj.problemId !== undefined) {
		if (!solutions[obj.problemId]) {
			obj.seeds = {}
			solutions[obj.problemId] = obj
		}
		solutions[obj.problemId].seeds[obj.seed] = {
			video: 		video,
			solution: 	obj.solution
		};
		video = [];
		lastfilled = null;
	} else {
		video.push(diffFrame(lastfilled, obj));
        lastfilled = obj.field.filled;
	}
});

rd.on('close', function () {
	var buggyCounter = -1;

	if (video.length > 0) {
		var obj = {
			problemId: 	buggyCounter,
			seed: 		'-1',
			solution: 	'<strong>ПРОБЛЕМА ОСТАНОВКИ НЕ РЕШЕНА</strong>'
		}
	    buggyCounter--;

		if (!solutions[obj.problemId]) {
			obj.seeds = {}
			solutions[obj.problemId] = obj
		}
		solutions[obj.problemId].seeds[obj.seed] = {
			video: 		video,
			solution: 	obj.solution
		};
		video = [];	
	}

	fs.writeFileSync('mc.json', JSON.stringify(solutions));
})


