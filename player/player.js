var sched = null;

function runMultik (error, solutions) {
	var video 		= []
	  , lastid      = null
	  , lastseed    = null;

	var game = null
	  , tour = null;

	$('#seedlist li').remove();

	d3.keys(solutions).forEach(function (solId) {
		lastid = solId;
		d3.keys(solutions[solId].seeds).forEach(function (seed) {
			$('#seedlist').append('<li><a href="javascript:">'+seed+'</li>')
			lastseed = seed;
		})
	})

	$('#seedlist a').click(function (ev) {
		lastseed = Number($(ev.target).text())
		setGame(lastid, lastseed)
	})

	function setGame(id, seed) {
		clearInterval(sched);
		game = solutions[id];
		tour = game.seeds[seed];
		$('#solid').text(game.problemId)
		$('#currseed').text(seed)
		$('#solution').html(tour.solution)
		playGame(true);
	}

	function playGame (onlyFirstFrame) {
		var CMARGIN = 4
		  , CSIZE   = 4;

		var hexagon = [
			[4, 0],
			[8, 3],
			[8, 7],
			[4, 10],
			[0, 7],
			[0, 3]
		]

		var video   	= tour.video
		  , framebuffer = {}
		  , svg     	= d3.select('#field')
		  , empties 	= [], x, y;

		CSIZE =  Math.min(4, 1150 / (video[0].field.width*8));

		for (x = 0; x < video[0].field.width; x++) 
			for (y = 0; y < video[0].field.height; y++) 
				empties.push({x:x, y:y})

		svg
			.attr('width',  video[0].field.width*(CSIZE*8.5+CMARGIN))
			.attr('height', video[0].field.height*(CSIZE*7.5+CMARGIN));

		function translate(ox, oy, x, y) {
			var xcorr = oy % 2 == 1 ? 4 : 0;
			return ((x+(ox*8+xcorr))*CSIZE+CMARGIN) + ',' + ((y+oy*7)*CSIZE+CMARGIN);
		}

		function drawHexes(selector, collection, style) {
			svg.selectAll(selector)
				.data(collection)
				.enter()
					.append('polygon')
					.attr('points', function (d) { 
						return hexagon
							.map(function (hp) { return translate(d.x, d.y, hp[0], hp[1]) })
							.join(' ') 
					})
					.attr('style', style)
			svg.selectAll(selector)
				.data(collection)
				.exit()
				.remove()
		} 

		drawHexes('.empty', empties, 'fill:none; stroke: #ccc;')

		function drawFrame (fr_i) {
			var fr = video[fr_i];

			svg.selectAll('polygon').remove();

			if (fr.field) { 				// Base frame
				framebuffer = {}
				fr.field.filled.forEach(function (f) {
					framebuffer[f.x+':'+f.y] = f
				})
			}

			if (fr.filled && fr.unfilled) { // Delta frame
				fr.unfilled.forEach(function (f) {
					delete framebuffer[f.x+':'+f.y] 
				})
				fr.filled.forEach(function (f) {
					framebuffer[f.x+':'+f.y] = f
				})
			}

			drawHexes('.empty',   empties, 		   'fill:none; stroke: #ccc;')
			drawHexes('.filled',  d3.values(framebuffer), 'fill: #60f; stroke: none;')
			drawHexes('.pivot',   [fr.unit.pivot], 'fill: none; stroke: red;')
			drawHexes('.members', fr.unit.members, 'fill: #fc3; stroke: none;')

			$('#steps').text('Step: ' + (fr_i+1) + '/' + video.length);
		}

		var fr_i  = 0

		drawFrame(fr_i);

		if (!onlyFirstFrame) {
			sched = setInterval(function () { 
				fr_i++; 
				if (video[fr_i]) 
					drawFrame(fr_i)
				else
					clearInterval(sched);
			}, 100)
		}

	}

	window.playGame = playGame;

	setGame(lastid, lastseed);

	$('#progress').text('Loaded.')
}

$(document).ready(function () {
	var solMax = 24
	  , solId  = 0;

	while (solId <= solMax) {
		$('#sollist').append('<li><a href="javascript:">'+solId+'</li>')
		solId++;
	}
	
	$('#sollist a').click(function (ev) {
		$('#progress').text('Loading...')
		d3.json('m_problem_'+Number($(ev.target).text())+'.json', runMultik);
	})

	$('#play-btn').click(function () { playGame(false) })
	
	$('#progress').text('Loading...')
	d3.json('m_problem_0.json', runMultik);
})